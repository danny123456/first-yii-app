# First Yii App
- Run docker-compose up -d in second_Yii_app
- Run docker-compose up -d in first_Yii_app
- Run docker-compose up -d in rabbit_worker
- Access to rabbit_worker container, run php worker.php
- Access to second_Yii_app php container, run php yii migrate.
- Open Postman, run api api/update-product, data:
{
    "name": "Adidas",
    "price": "20"
},
method: POST

