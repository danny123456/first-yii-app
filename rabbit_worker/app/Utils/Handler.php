<?php
namespace App\Utils;

class Handler
{
    public function notification($msg)
    {
        try {
            //open write log
            $log = new Logging();
            $log->lfile(dirname(dirname(__DIR__)) . '/worker_log.txt');
            $messageData = $msg->body;

            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            $deData = json_decode($messageData);
            $postfields = $deData->dataPost;
            $host = $deData->host;

            //write the token
//        $log->lwrite('Token: ' . $token);
            //write log data
            $log->lwrite('Data: ' . $messageData);

            if ($host != "") {
                $url = $host . "/product/create";

                $query_string = "";
                foreach ($postfields AS $k => $v) {
                    $query_string .= "$k=" . urlencode($v) . "&";
                }
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
                curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6');
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                "Authorization: $token"
//            ));
                $resultRest = curl_exec($ch);

                //write log of result
                $log->lwrite('API Result: ' . $resultRest);
                $log->lwrite('/*-------------------------------------------------------------------------------*/');
                if (curl_error($ch)) {
                    $log_error = new Logging();
                    $log_error->lfile(dirname(dirname(__DIR__)) . '/worker_error.txt');
                    $log_error->lwrite('/*-------------------------------------------------------------------------------*/');
                    $log_error->lwrite('Error curl' . curl_errno($ch) . ' - ' . curl_error($ch));
                    $log_error->lwrite('Query string' . $messageData);
                    $log_error->lwrite('/*-------------------------------------------------------------------------------*/');
                    $log_error->lclose();
                    die("Connection Error: " . curl_errno($ch) . ' - ' . curl_error($ch));
                }

                curl_close($ch);
            }
        } catch(\Exception $e) {
            $log_error = new Logging();
            $log_error->lfile(dirname(dirname(__DIR__)) . '/worker_error.txt');
            $log_error->lwrite('Error callback' . $e->getMessage());
            $log_error->lclose();
        }
    }
}