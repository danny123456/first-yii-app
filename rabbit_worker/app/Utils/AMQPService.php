<?php
namespace App\Utils;

use App\interfaces\IAMQPService;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AMQPService implements IAMQPService
{
    private $connection;

    public function __construct($config = array())
    {
        $this->connection = new AMQPStreamConnection(
            $config['rabbit_host'],
            5672,
            $config['rabbit_user'],
            $config['rabbit_password']
        );
    }

    public function consume($argv, $callback)
    {
        $channel = $this->connection->channel();
        $channel->exchange_declare('direct_exchange', 'direct', false, false, false);
        list($queueName) = $channel->queue_declare('', false, false, true, false);

        $tasks = array_slice($argv, 1);

        if (empty($tasks)) {
            $tasks = ['notification'];
        }

        foreach ($tasks as $task) {
            $channel->queue_bind($queueName, 'direct_exchange', $task);
        }

        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queueName, '', false, false, true, false, $callback);
        while ($channel->is_consuming()) {
            $channel->wait();
        }

        $channel->close();
        $this->connection->close();
    }
}