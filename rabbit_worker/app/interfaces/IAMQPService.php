<?php
namespace App\interfaces;

interface IAMQPService
{
    public function consume($argv, $callback);
}