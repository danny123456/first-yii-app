<?php
require_once __DIR__.'/vendor/autoload.php';

use App\Utils\AMQPService;
use App\Utils\Handler;
use App\Utils\Logging;

$handler = new Handler();
$callback = function($msg) use ($handler) {
    $routingKey = $msg->delivery_info['routing_key'];
    if ($routingKey == 'notification') {
        $handler->notification($msg);
    }
};

try {
    $config = parse_ini_file("Config/config.ini");
    //connect to rabbit
    $amqpService = new AMQPService($config);
    $amqpService->consume($argv, $callback);
} catch(\Exception $e) {
    $log_error = new Logging();
    $log_error->lfile(__DIR__ . '/worker_error.txt'); 
    $log_error->lwrite($e->getMessage());
    $log_error->lclose();
}