<?php
namespace app\services;

interface IAMPQService
{
    public function sendToQueue($data, $routingKey);
}