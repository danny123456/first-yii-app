<?php
namespace app\services;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class AMPQService implements IAMPQService
{
    private $connection;

    public function __construct()
    {
        $params = \Yii::$app->params;
        $this->connection = new AMQPStreamConnection(
            $params['rabbitParams']['rabbitHost'],
            $params['rabbitParams']['rabbitPort'],
            $params['rabbitParams']['rabbitUserName'],
            $params['rabbitParams']['rabbitPassword']
        );
    }

    public function sendToQueue($data = [], $routingKey = '')
    {
        if (!empty($data)) {
            $channel = $this->connection->channel();
            $channel->exchange_declare('direct_exchange', 'direct', false, false, false);
            $msg = new AMQPMessage(json_encode($data), array('delivery_mode' => 2));
            $channel->basic_publish($msg, 'direct_exchange', $routingKey);

            $channel->close();
            $this->connection->close();
            return true;
        }

        return false;
    }
}