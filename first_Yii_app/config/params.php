<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'rabbitParams' => [
        'rabbitUserName' => 'guest',
        'rabbitPassword' => 'guest',
        'rabbitHost' => 'rabbitmq_container',
        'rabbitQueue' => 'ampq',
        'rabbitPort' => 5672
    ]
];
