<?php
namespace app\controllers;

use yii\rest\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    public function _sendResponse($statusCode = 200, $data = [], $message = '')
    {
        $response = \Yii::$app->response;
        $response->setStatusCode($statusCode);
        $response->format = Response::FORMAT_JSON;
        if ($message) {
            $data['message'] = $message;
        }

        $response->data = $data;

        return $response;
    }
}