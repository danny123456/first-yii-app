<?php
namespace app\controllers;

use app\services\IAMPQService;

class ApiController extends BaseController
{
    private $service;

    public function __construct($id, $module, $config = [], IAMPQService $service)
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    public function actionUpdateProduct()
    {
        $request = \Yii::$app->request;
        if ($request->isPost) {
            $data = [
                'dataPost' => $request->post(),
                'host' => 'second_mvc_nginx'
            ];

            $this->service->sendToQueue($data, 'export');

            return $this->_sendResponse(200, $request->post(), 'Product is updated successfully!');
        }

        return $this->_sendResponse(405, [], 'Method is not allowed!');
    }
}