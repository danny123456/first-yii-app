<?php
namespace app\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class TaxBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'calculateTax',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'calculateTax'
        ];
    }

    public function calculateTax($event)
    {
        $event->sender->price = $event->sender->price + $event->sender->price * 10/100;
    }
}