<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200506_233643_products
 */
class m200506_233643_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'price' => Schema::TYPE_DECIMAL
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200506_233643_products cannot be reverted.\n";
        $this->dropTable('products');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200506_233643_products cannot be reverted.\n";

        return false;
    }
    */
}
