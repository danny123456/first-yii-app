<?php
namespace app\interfaces;

use app\models\Users;

interface IAuthService
{
    public function getToken(Users $user);
    public function validate($data = []);
}