<?php
namespace app\interfaces;

interface IService
{
    public function getList();
    public function update($data = []);
    public function delete($data = []);
}