<?php
namespace app\interfaces;

interface IRepository
{
    public function getList();
    public function update($data = []);
    public function delete($data = []);
}