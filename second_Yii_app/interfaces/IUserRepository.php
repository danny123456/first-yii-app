<?php
namespace app\interfaces;

interface IUserRepository
{
    public function createUser($data = []);
}