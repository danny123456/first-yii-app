<?php
namespace app\commands;

use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = \Yii::$app->authManager;
        $auth->removeAll();

        $createProduct = $auth->createPermission('createProduct');
        $createProduct->description = 'Create product';
        $auth->add($createProduct);

        $deleteProduct = $auth->createPermission('deleteProduct');
        $deleteProduct->description = 'Delete Product';
        $auth->add($deleteProduct);

        $productOwnerRole = $auth->createRole('productOwner');
        $productOwnerRole->description = 'Product Owner';
        $auth->add($productOwnerRole);
        $auth->addChild($productOwnerRole, $createProduct);

        $adminRole = $auth->createRole('admin');
        $adminRole->description = 'Admin';
        $auth->add($adminRole);
        $auth->addChild($adminRole, $productOwnerRole);
        $auth->addChild($adminRole, $deleteProduct);
    }
}