<?php
namespace app\repositories;

use app\interfaces\IRepository;
use app\models\Product;
use yii\db\StaleObjectException;

class ProductRepository implements IRepository
{
    public function getList()
    {
        return Product::find()->asArray()->all();
    }

    public function update($data = [])
    {
        $product = new Product();
        if (isset($data['id'])) {
            $product = Product::findOne($data['id']);
        }

        foreach ($data as $name => $value) {
            $product->$name = $value;
        }

        try {
            $product->save();

            return $product;
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function delete($data = [])
    {
        if (isset($data['id'])) {
            $product = Product::findOne($data['id']);

            try {
                $product->delete();

                return $data;
            } catch (\Throwable $e) {
                return false;
            }
        }

        return false;
    }
}