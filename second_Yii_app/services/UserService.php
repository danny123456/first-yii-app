<?php
namespace app\services;

use app\interfaces\IUserRepository;
use app\interfaces\IUserService;

class UserService implements IUserService
{
    private $repository;

    public function __construct(IUserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createUser($data = [])
    {
        return $this->repository->createUser($data);
    }
}