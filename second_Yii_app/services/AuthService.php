<?php
namespace app\services;

use app\interfaces\IAuthService;
use app\models\Users;

class AuthService implements IAuthService
{
    public function getToken(Users $user)
    {
        $jwt = \Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();

        $token = $jwt->getBuilder()
            ->issuedAt($time)// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $user->id)// Configures a new claim, called "uid"
            ->getToken($signer, $key); // Retrieves the generated token

        return (string) $token;
    }

    public function validate($data = [])
    {
        if (empty($data)) {
            return false;
        }

        try {
            $user = Users::find()->where([
                'username' => $data['username'],
                'status' => Users::STATUS_ACTIVE
            ])->one();
            if (!$user) {
                return false;
            }

            if (!\Yii::$app->getSecurity()->validatePassword($data['password'], $user->password)) {
                return false;
            }

            return $user;
        } catch (\Exception $e) {
            return false;
        }
    }
}