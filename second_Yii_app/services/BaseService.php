<?php
namespace app\services;

use app\interfaces\IRepository;

class BaseService
{
    protected $repository;

    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }
}