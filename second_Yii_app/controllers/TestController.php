<?php
namespace app\controllers;

use yii\rest\Controller;

class TestController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => 'app\components\MyFilter',
            'only' => ['data']
        ];
        return $behaviors;
    }

    public function actionData()
    {

    }
}