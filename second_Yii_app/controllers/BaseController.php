<?php
namespace app\controllers;

use app\interfaces\IService;
use yii\rest\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    protected $service;

    public function __construct($id, $module, $config = [], IService $service)
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    public function _sendResponse($statusCode = 200, $data = [], $message = '')
    {
        $response = \Yii::$app->response;
        $response->setStatusCode($statusCode);
        $response->format = Response::FORMAT_JSON;
        if ($message) {
            $data['message'] = $message;
        }

        $response->data = $data;

        return $response;
    }
}